﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace InterviewApp
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private Color _boxColor = Color.Green;
        public Color BoxColor
        {
            get => _boxColor;
            set
            {
                if (value != this._boxColor)
                {
                    _boxColor = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand ChangeBoxColorCommand { get;  }

        public MainPageViewModel() 
        {
            ChangeBoxColorCommand = new Command<string>((c) => ExecuteChangeBoxColor(c));
        }

        private void ExecuteChangeBoxColor(string color)
        {
            switch (color)
            {
                case "Red":
                    BoxColor = Color.Red;
                    break;
                case "Blue":
                    BoxColor = Color.Blue;
                    break;
                case "Green":
                default:
                    BoxColor = Color.Green;
                    break;

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
